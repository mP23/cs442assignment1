CS542 Design Patterns
Spring 2016
PROJECT <1> README FILE

Due Date: <Monday, February 1, 2016>
Submission Date: <Monday, February 1, 2016>
Grace Period Used This Project: <0> Days
Grace Period Remaining: <3> Days
Author(s): <Matthew Price> [and <OTHER FULL NAME>, if group]
e-mail(s): mprice3@binghamton.edu> [and <OTHER E-MAIL>, if group]


PURPOSE:

[
  To find the most frequently occuring element names in an XML file
]

PERCENT COMPLETE:

[
  I have completed 100% of my responsibilities for this project.
]

PARTS THAT ARE NOT COMPLETE:

[
  Everything is 100% complete, however, I should have documented my code more.
]

BUGS:

[
  I have no bugs in my program.
]

FILES:

[ 
  Included with this project are 3 files:

  Driver.java, the file which runs StringOperations.java
  StringOperations.java, the file responsible for processing the XML file, extracting the element names, and calculating the most frequent ones
  FileProcessor.java, the file that reads any input given to it line by lin
  README, the text file you are presently reading
]

SAMPLE OUTPUT:

[
  g7-34:~/DP/Matthew_Price_assign1/Starting-with-java/src> java Driver amazon.wsdl
  The most frequently occuring element is ASIN.
  It occurs 15 times.
]

TO COMPILE:

[
  Just extract the files and then run javac *.java
]

TO RUN:

[
  Run java Driver amazon.wsdl
]

JUSTIFICATION FOR DATA STRUCTURE:

[ 
  I used an array list to keep track of the strings because I felt it was quite efficient and dynamic. Array lists are easy to work with, especially when you're doing string operations. 
]
EXTRA CREDIT:

[
  N/A
]


BIBLIOGRAPHY:

This serves as evidence that we are in no way intending Academic Dishonesty.
<NAMES OF GROUP MEMBERS>

[
  *http://www.tutorialspoint.com/java/java_regular_expressions.htm
  *http://www.tutorialspoint.com/java/java_arraylist_class.htm
  *http://www.tutorialspoint.com/java/java_strings.htm
  *http://www.tutorialspoint.com/java/io/java_io_reader.htm
  *http://www.tutorialspoint.com/java/java_files_io.htm
]

ACKNOWLEDGEMENT:

[
 N/A
]
