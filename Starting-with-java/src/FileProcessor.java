import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.FileReader;
import java.io.*;
class FileProcessor {
	String fileName;
	public FileProcessor(String fileName) {
		this.fileName = fileName;
	}
	//returns String 
	public String readLineFromFile() {
		//returns one line of the file as a String at a time
		String lineToRead = " ";
		StringBuilder sb = new StringBuilder();
		try (BufferedReader br = new BufferedReader(new FileReader(new File(fileName)))) {
                        while ((lineToRead = br.readLine()) != null) {
                                sb.append(lineToRead);
                        }
		} catch (IOException e) {
			e.printStackTrace();	
		}
		//return lineToRead.toString();
		return lineToRead;
	}
}
